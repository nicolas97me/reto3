import CustomerDBAdapterEPS1
import SaleAPIAdapter1
#Add new import adapter
import CustomerCSVAdapertIPS2

import json

def adapt(datagramMessageJson):
    datagramMessageJson=json.loads(datagramMessageJson)
    print(type(datagramMessageJson))
    
    idEmpresa = datagramMessageJson['idEmpresa']
    print(idEmpresa)
    register = datagramMessageJson['register']
    print(register)
    
    if(idEmpresa == "EPS_1"):
        json_data = CustomerDBAdapterEPS1.adapt(register)
        return json_data
    elif (idEmpresa == "1"):
        json_data = SaleAPIAdapter1.adapt(register)
        return json_data
    elif (idEmpresa == "IPS2"):
        json_data = CustomerCSVAdapertIPS2.adapt(register)
        return json_data
        
    # new compamy: add here corresponding elif branch, and create a new adapter
    else:
        # error
        return "Error: Empresa no integrada aun: " + str(idEmpresa)