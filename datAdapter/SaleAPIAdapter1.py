import json

def adapt(entrada_json):
    mapeo_campos = {
    "idConsolidado": "idFactura",
    "fecha": "fechaVenta",
    "valor": "monto",
    "cliente":"idCliente",
    "medioPago": "medioPago"
    }
    
    datos = json.loads(entrada_json)
    datos_transformados = {}

    for campo_entrada, campo_salida in mapeo_campos.items():
        if campo_entrada in datos:
            datos_transformados[campo_salida] = datos[campo_entrada]
    return datos_transformados
