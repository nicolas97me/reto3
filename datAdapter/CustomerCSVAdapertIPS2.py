import csv
import json

def adapt(csv_data):
    print("tiipo ADAPT",type(csv_data))
    
    field_order = ["identificacion", "nombre", "razonSocial","tipoCte","dir","correo","telefono"]
    
    csv_reader = csv.reader(csv_data.splitlines(), delimiter=',')
    json_data = []
        
    for row in csv_reader:
        json_item = {}
        for i, field_name in enumerate(field_order):
            if field_name == "identificacion":
                json_item["identificacion"] = row[i]
            
            elif field_name == "nombre":
                json_item["nombre"] = row[i]
                
            elif field_name == "razonSocial":
                json_item["idCliente"] = row[i]
                
            elif field_name == "tipoCte":
                json_item["tipoCliente"] = row[i]
                   
            elif field_name == "correo":
                json_item["email"] = row[i]
                    
            elif field_name == "telefono":
                json_item["telefono"] = row[i]
                
            elif field_name == "dir":
                json_item["direccion"] = row[i]
           
        
        json_data.append(json_item)
            
        return json_data
    